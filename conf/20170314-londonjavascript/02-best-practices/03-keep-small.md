## Keep'em small

```javascript
const Users = props => (
  <section>
    <div><h1>Users</h1></div>
    <div>
      { props.users.map(user => 
      	<div class="user">
      		<span class="username">{ user.username }</span>
      		<span class="firstname">{ user.firstname }</span>
      		<span class="lastname">{ user.lastname }</span>
      		...
      	</div>
      }
    </div>
  </section>
);
```

Note:

Kind of obvious, but worth mentionning
Much easier to reason with smaller components
If you often have more than 100 lines then might have a problem
Never seen a project where components are "too small"

This most obvious example, responsible for rendering the list
and every user element. Super easy to split
## Keep'em small

```javascript
const Users = props => (
  <section>
    <div><h1>Users</h1></div>
    <div>
      { props.users.map(user => <User user />}
    </div>
  </section>
);

const Users = props => (
  <div class="user">
    <span class="username">{ props.user.username }</span>
  </div>
);
```

Note:

Component responsible for the list does not care 
how the user is rendered

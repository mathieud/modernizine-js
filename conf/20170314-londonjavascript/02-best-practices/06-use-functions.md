## Functional Components

```javascript
const Welcome = props => (
  <h1>Hello, {this.props.name}</h1>
);
```

```javascript
class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}
```

Note:

This does the same

Functional form is smaller, recommended by most and prevents you from putting too may things

I usually use classes, had problems with hot reload, and allows to break down
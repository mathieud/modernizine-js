## React is **only** for Views

```php
<?php

echo '<h1>Users</h1>';
 
$sql = "SELECT * FROM users";
$result = mysql_query($sql) or die(mysql_error());
 
echo '<table class="user-table">';
while($row = mysql_fetch_assoc($result)){
    echo '<tr>';
    echo '<td>' . $row['username'] . '</td>';
    echo '<td>' . $row['date_registered'] . '</td>';
    echo '</tr>';
}
echo '</table>';
```

Note:
Do not put business (or any) logic in it, or you will fall back to the dark age of... flat php and jsp's
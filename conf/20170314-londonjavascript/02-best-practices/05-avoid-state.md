## Avoid using the state

* easy to add logic
* hard to test
* can't share information

Note:

Describe what he state is if wasn't done by previous talk

As said, view library: no logic

State makes it too easy to put logic in the component
Brings super weird behaviour, where a component re-renders itself for no apparent reason
Because it changes it's state

State is internal to the component, hard to modify it to test (props EZ)

The state is local, the rest of you app can't access what's in it

Don't never use it, but beware
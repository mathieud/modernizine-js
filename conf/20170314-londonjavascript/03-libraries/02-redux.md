# Redux

* Very popular Flux implementation
* Single Store
* Time travelling!

Note:

Redux is probably the most popular implementation of the Flux pattern
One store, contains the entire state of your app
Reducers take actions as input and update the store (called Reducers because they reduce a list of actions to the store)
Time travelling debugging
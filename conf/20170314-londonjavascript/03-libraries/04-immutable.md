## Immutable.js

* Immutable data structure
  * List, Map, Record
* PureComponents
  * override `shouldComponentUpdate`
* Careful when manipulating plain js vs immutable

Note:

Immutable is a library that offers immutable data structure
Everytime the object change, the value of the current object is untouched
A new object is created

Allows you to use Reacts PureComponent

Never use toJS() in a connect: every keystrike took ~1sec...
had to git bisect to find the origin
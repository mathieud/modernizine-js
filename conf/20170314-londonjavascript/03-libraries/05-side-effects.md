## Side effects

* Custom middleware for simple ones
* redux-saga if needed

Note:

Don't like putting code in the actions, prefer create a middleware
Saga is great, but overkill 90% of the time - if you can live with a setTimeout do it
## ES6 Modules

```javascript
import React from 'react';

class Label extends React.Component {
  render() {
    return (
      <section className="..." >
        <h3 className="...">{translate(this.props.label)}</h3>
      </section>
    )
  }
}
Label.displayName = 'label';
Label.propTypes = {
  label: React.PropTypes.array.isRequired
};

export default Label;
```

Note:

So much better! At least looks like something
Not perfect though (translate still global)
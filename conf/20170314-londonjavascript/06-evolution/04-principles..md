## Principles

  - Think Big
  - Start Small
  - Go Fast

Note:

3 principles, that very often cited when talking about very successful product or startups
Applies very well to a product evolution


- In an ideal world, how should the project look like?
 - Chose your battles, not everything is worth fixing (find xkcd how long*how often matrix?)
- Don't try to do everything at once, baby steps are the best
 - Can you find any quick wins?
 - What's the biggest pain point?
 - Rewrite App? Get 80% coverage? Really hard
 - Move from svn to git? Introduce unit tests? EZ
- No stock!
 - Don't delay
 - Go to Prod as fast as possible

## Start Small

* JS modules
* Linted
* Unit tested
* Up to date React version
* Data flow management framework (redux...)


Note:

- Don't try to do everything at once, baby steps are the best
 - Can you find any quick wins?
 - What's the biggest pain point?
 - Rewrite App? Get 80% coverage? Really hard
 - Move from svn to git? Introduce unit tests? EZ


The most critical to me: 
- Hate coffeescript
- No control over dependencies
- High risk of collision
- Prevents any attempt at unit test

## Coverage

```javascript
"jest": {
	...,
    "collectCoverage": true,
    "collectCoverageFrom": [
      "src/**/*.{js,jsx}"
    ],
    "coverageThreshold": {
      "global": {
        "statements": 80,
        "branches": 80,
        "lines": 80,
        "functions": 80
      }
    }
  }
```

Note:

Super easy to set up
Actually fails the build since Jest19

Same thing easily achieved with Istanbul
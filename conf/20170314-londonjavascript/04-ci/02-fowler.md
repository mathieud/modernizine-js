## CI

* Daily (at least) push to master
* Automated tests and build on every commit
* Back to green within 10 minutes

[©JezHumble](https://martinfowler.com/bliki/ContinuousIntegrationCertification.html)

Note:

Continuous delivery Guru
Featured on MartinFowler's blog
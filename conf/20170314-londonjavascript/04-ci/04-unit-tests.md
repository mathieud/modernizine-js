## Unit Tests

* Jest
* Mocha + Chai

* Enzyme

* Snapshots

Note:

Facebook has made it really easy to test react components
Jest in plug and play

Enzyme is really good to render and manipulate React components

Snapshot testing compares react rendered object to previous one, displays nice diff

E2E testing very painful
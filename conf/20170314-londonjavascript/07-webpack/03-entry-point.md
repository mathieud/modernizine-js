## Webpack

```javascript
// webpack.config.js

module.exports = {
  entry: {
    'Label': './src/Label.jsx',
  },
  ...,
  externals: {
    "react": "React",
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
  }
};
```

Note:

Find out exactly what externals does
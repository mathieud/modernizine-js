## Build

```javascript
// webpack.config.js

module.exports = {
  entry: {
    'Label': './src/Label.jsx',
  },
  ...,
  output: {
      path: path.join(__dirname, "../../www/js"),
      filename: "webpack-[name].js",
      library: "[name]",
  },
  ...
};
```

Note:

Find out exactly what livrary does
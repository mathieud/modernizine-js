## Think Big

- State of the art ES6 modules - ok
- -> Linted
- -> Unit tested
- 80%+ coverage
- Up to date React version
- Redux

Note:

Perfect? Not at all
Better? So much!
Way forward? EZ!
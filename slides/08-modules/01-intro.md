## ES6 Modules

- Introduced in ES6 (javascript 2015)
- Encapsulate your code
- Expose what you want
- Control what you import
## ES6 Modules

```javascript
label =
  displayName: 'label'
  propTypes:
    label: React.PropTypes.string.isRequired

  render: ->
    <section>
      <h3 className="...">{translate(@props.label)}</h3>
    </section>

Label = React.createClass label
```

Note:

What our components looked like...
This one pretty simple

React and translate from the global namespace
Label dumped in the global namespace
CoffeeCaca
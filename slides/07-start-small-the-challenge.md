## The challenge

<ul>
<li>Work with existing components</li>
<li>Integrate to the current build workflow</li>
<li>Not time consuming</li>

Note:

Gradual rewrite: be able to write new component, use the existing ones, and be used from existing ones
Don't want to change the entire build workflow (Gulp)
Should take at least time as possible: Ideally in a couple of hours, after work, with a beer
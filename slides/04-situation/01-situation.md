## The project

- 100s of React components
- Written in Coffescript...
- Everything dumped in the global namespace
- No dependency injection / control on dependencies
- No unit test
- Lots of logic in the components
- Outdated React version, impossible to bump *
- ...
- :'(

Note:
 - Project had been going on for 6 months with 2-3 devs
 - More opiniated (I don't like Ruby), coffee was half decent before es6
 - Every component dumped in the global namespace, build only aggregated all the files
 - All the globals made it a nightmare to (even try) to unit test
 - Fetching data in React events methods -> make sure everything re-redered 4/5 times everytime something changes -> hary bugs
 - Early React, changed super quickly, really hard to catch up for, impossible to do at once
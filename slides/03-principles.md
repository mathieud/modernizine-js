## Principles

 <p class="fragment fade-in">
  - Think Big
</p>
 <p class="fragment fade-in">
  - Start Small
</p>
 <p class="fragment fade-in">
  - Go Fast
</p>

Note:

- Don't try to do everything at once, baby steps are the best
 - Can you find any quick wins?
 - What's the biggest pain point?
 - Rewrite App? Get 80% coverage? Really hard
 - Move from svn to git? Introduce unit tests? EZ
- No stock!
 - Don't delay
 - Go to Prod as fast as possible

## Start Small

<section>
<ul>
<li>Keep previous list in mind</li>
<li>Identify biggest pain point</li>
<li>Dependency between them?</li>
</ul>
</section>
<section>
<li>Treat this as an experiment</li>
</section>

<section>
<ul class="fragment fade-out">
<li>Unit tested</li>
<li>Up to date React version</li>
<li>Data flow management framework (redux...)</li>
</ul>
</section>


Note:

You can't fix everything at once
Can you find a quick win
Will fixing one make the others much easier?

The most critical to me: 
- Hate coffeescript
- No control over dependencies
- High risk of collision
- Prevents any attempt at unit test

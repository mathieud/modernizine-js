## And then...

```javascript
// webpack.config.js

module.exports = {
  entry: {
    'DocumentLibrary': './src/document-library/DocumentLibrary.jsx',
    'Label': './src/Label.jsx',
  },
  output: {
      path: path.join(__dirname, "../../www/js"),
      filename: "webpack-[name].js",
      library: "[name]",
  },
  ...,
  externals: {
    "react": "React",
    "lodash": "_",
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
  }
};
```

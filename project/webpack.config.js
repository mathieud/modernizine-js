var webpack = require('webpack');
var path = require('path');

var config = {
  entry: {
    'Label': './src/label.js'
  },
  output: {
    path: './build',
    filename: 'label.js',
    library: "[name]",
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
};

module.exports = config;

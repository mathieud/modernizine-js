import React from 'react';

class Label extends React.Component {

  render() {
    var labels = this.props.label.map(function(item){ return (<span>{item}</span>) })
      return (
        <section className="fundsheet-widget fundsheet-widget-awards" data-tabs="overview" >
          <h3 className="fundsheet-widget-title">{translate('FUNDSHEET_LABEL_TITLE')}</h3>
          <div>{labels}</div>
            </section>
            )
      }
}
Label.displayName = 'label';
Label.propTypes = {
  label: React.PropTypes.array.isRequired
};

module.exports = Label;
